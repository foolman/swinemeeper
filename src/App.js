import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import R from 'ramda';
import genBoard from './mapGenerator';
import rnd from './random';

class Field extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    const {reveal, lock, field, hover, hovered} = this.props;
    const cn = field.revealed ? `field revealed ${field.kind}` : `field ${field.locked ? 'locked' : ''}`;
    const revealButton = field.revealed || field.locked ? null : (<span className="reveal" onClick={() => reveal()}>R</span>);
    const lockButton = field.revealed ? null : (<span className="lock" onClick={() => lock()}>X</span>);
    return hovered ?
        (<span className={cn}><span className="label">{field.mines}</span>{revealButton}{lockButton}</span>) :
        (<span className={cn} onMouseOver={()=>hover()}><span className="label">{field.mines}</span></span>);

  }
}

const Row = ({children}) => (<div className="row" >{children}</div>);
const Table = ({board, click, lock, hover, hoverX, hoverY}) => {
  const field = (row) => (kind, col) => (<Field key={`f-${row}-${col}`} field={kind}
                                                reveal={()=>click(col,row)}
                                                lock={() => lock(col,row)}
                                                hover={() => hover(col, row)}
                                                hovered={hoverX === col && hoverY === row}
  />);
  const fields = (rowIdx) => R.addIndex(R.map)(field(rowIdx));
  const rows = R.addIndex(R.map)((row,idx) => (<Row key={`r-${idx}`}>{fields(idx)(row)}</Row>));
  return (<div>{rows(board)}</div>);
};


class App extends Component {
  constructor(props){
    super(props);
    let seed = 20170512;
    let board;
    const random = rnd(seed);
    while(true){
      try{
        board = genBoard(10, 10, random);
        break;
      }catch(e){
        if (e !== 'not done in 100 moves, aborting') throw e;
        console.log('Exception thrown', e);
        seed += 142130;
      }
    }
    this.state = {board: board };
  }

  click(x,y){
    console.log(`clicked on [${x}, ${y}]`);
    const board = this.state.board.map.slice(0);
    board[y][x].revealed = !board[y][x].revealed;
    const newBoard = Object.assign({}, this.state.board, {map : board});
    this.setState({board: newBoard});
  }

  lock(x,y){
    console.log(`locked ${x}, ${y}`);
    const board = this.state.board.map.slice(0);
    board[y][x].locked = !board[y][x].locked;
    const newBoard = Object.assign({}, this.state.board, {map : board});
    this.setState({board: newBoard});
  }

  hover(x,y){
    console.log(`hovered ${x}, ${y}`);
    if (this.state.board.hoverX === x && this.state.board.hoverY === y) return;
    console.log('creating new board');
    const newBoard = Object.assign({}, this.state.board, {hoverX:x, hoverY:y});
    this.setState({board: newBoard});
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Table board={this.state.board.map} hoverX={this.state.board.hoverX} hoverY={this.state.board.hoverY}
               click={(x,y) => this.click(x,y)}
               lock={(x,y) => this.lock(x,y)} hover={(x,y) => this.hover(x,y)}/>
      </div>
    );
  }
}

export default App;
