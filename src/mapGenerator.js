/**
 * Created by foolman on 21/05/17.
 */
"use strict";

import R from 'ramda';

const isNotOccupiedFn = (width, height) => (map, x, y) => {
  if (x < 0 || x >= width) return true;
  if (y < 0 || y >= height) return true;
  return (!map[y][x].visited)
};

const isVisitableFn = (width, height) => (map, x,y) => {
  if (x < 0 || x >= width) return false;
  if (y < 0 || y >= height) return false;
  return !map[y][x].visited && map[y][x].visitCount < 3;
};

const validMovesFn = (width, height) => {
  const isVisitable = isVisitableFn(width, height);
  const isNotOccupied = isNotOccupiedFn(width, height);
  return (map, x,y) => {
    const validMoves = [];
    if (isVisitable(map, x-1, y) && isNotOccupied(map, x-2, y)
        && isNotOccupied(map, x-1, y-1) && isNotOccupied(map, x-1, y+1)
    ) validMoves.push([x-1, y]);
    if (isVisitable(map, x+1, y) && isNotOccupied(map, x+2, y)
        && isNotOccupied(map, x+1, y-1) && isNotOccupied(map, x+1, y+1)) validMoves.push([x+1, y]);
    if (isVisitable(map, x, y-1) && isNotOccupied(map, x, y-2)
        && isNotOccupied(map, x-1, y-1) && isNotOccupied(map, x+1, y-1)) validMoves.push([x, y-1]);
    if (isVisitable(map, x, y+1) && isNotOccupied(map, x, y+2)
        && isNotOccupied(map, x-1, y+1) && isNotOccupied(map, x+1, y+1)) validMoves.push([x, y+1]);
    return validMoves;
  };
};
const kinds = {
  ENTRY : 'entry',
  EXIT: 'exit',
  MINE: 'mine',
  ROOM: 'r',
  EXTRA: 'e',
  UNKNOWN: 'unknown'
};

const fieldValue = (width, height, map) => (x,y) => {
  if (x < 0 || x >= width) return kinds.UNKNOWN;
  if (y < 0 || y >= height) return kinds.UNKNOWN;
  return map[y][x].kind;
};

const genBoard = (width, height, random) => {
  const entry = random(height);
  const exit = random(height);
  const board = {};
  board.entry=[0,entry];
  board.exit=[width-1, exit];
  board.position = [0, entry];

  const wIdx = R.range(0,width);
  const hIdx = R.range(0,height);
  const emptyCell = () => ({kind:kinds.MINE, visited: false, visitCount: 0, locked: false});
  const emptyMap = () => R.map(() => R.map(emptyCell, wIdx), hIdx);
  const map = emptyMap();

  const validMoves = validMovesFn(width, height);

  map[0][entry].visited = true;
  map[0][entry].revealed = true;
  let pos = [entry,0];
  const poss = [];
  let counter = 0;
  while (pos[0] !== exit || pos[1] !== width - 1 ){
    let moves = validMoves(map, pos[0], pos[1]);
    while (moves.length === 0){
      const newPos = poss.pop();
      moves = validMoves(map, newPos[0], newPos[1]);
      map[pos[1]][pos[0]].visited = false;
      map[pos[1]][pos[0]].kind = kinds.MINE;
      pos = newPos;
    }
    poss.push(pos);
    pos = moves[random(moves.length)];
    map[pos[1]][pos[0]].visited = true;
    map[pos[1]][pos[0]].kind = kinds.ROOM + counter;//counter;
    map[pos[1]][pos[0]].visitCount++;
    counter++;
    if (counter === 100) {
      throw 'not done in 100 moves, aborting';
    }
  }
  for (pos of poss){
    let moves = validMoves(map, pos[0], pos[1]);
    while(moves.length){
      pos = moves[random(moves.length)];
      map[pos[1]][pos[0]].visited = true;
      map[pos[1]][pos[0]].kind = kinds.EXTRA + counter;
      counter ++;
      moves = validMoves(map, pos[0], pos[1]);
    }
  }
  map[0][entry].kind = kinds.ENTRY;
  map[width-1][exit].kind = kinds.EXIT;
  board.map = map;
  const fv = fieldValue(width, height, map);
  const minesAround =  (x,y) => {
    const neighbours = [[-1,-1],[-1,0],[-1,1], [0,-1],[0,1], [1,-1], [1, 0], [1,1]];
    const indexValues = R.map((i) => [i[0]+x, i[1]+y], neighbours);
    return R.reduce((acc, i) => acc + (fv(i[0],i[1]) === kinds.MINE ? 1 : 0), 0, indexValues);
  };
  R.map((y) => R.map((x) => map[y][x].mines = minesAround(x,y) , wIdx), hIdx);
  board.hoverX = -1;
  board.hoverY = -1;
  return board;
};

export default genBoard;
