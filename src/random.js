/**
 * Created by foolman on 21/05/17.
 */
"use strict";
const rnd = (seed) => {
  let lastValue = seed;
  return (range) => {
    let value = lastValue * 137;
    value ^= 0xf425;
    value += (value >> 8) + ((value | 0xff) << 8);
    lastValue = value;
    return Math.abs(lastValue % range);
  }
};

export default rnd;
